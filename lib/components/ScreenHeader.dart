import 'package:flutter/material.dart';

class ScreenHeader extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      actions: <Widget>[
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.filter_list,
            color: Colors.grey.shade700,
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.shopping_cart,
            color: Colors.grey.shade700,
          ),
        ),
      ],
      backgroundColor: Colors.white70,
      leading: IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.menu,
            color: Colors.grey.shade700,
          )),
      title: Text(
        'Shopping',
        style: TextStyle(
          color: Colors.black87,
        ),
      ),
      centerTitle: true,
      bottom: _buildBottomBar(),
    );
  }

  PreferredSize _buildBottomBar() {
    return PreferredSize(
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Card(
          child: Container(
            child: TextField(
              decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: IconButton(onPressed: () {}, icon: Icon(Icons.search)),
                  suffixIcon:
                      IconButton(onPressed: () {}, icon: Icon(Icons.mic))),
            ),
          ),
        ),
      ),
      preferredSize: Size.fromHeight(80.0),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(140.0);
}
