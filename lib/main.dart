import 'dart:io';
import 'package:appecomerce/components/ScreenHeader.dart';
import 'package:appecomerce/screens/ScreenFavorites.dart';
import 'package:appecomerce/screens/ScreenLocation.dart';
import 'package:appecomerce/screens/ScreenNotification.dart';
import 'package:appecomerce/screens/ScreenSetting.dart';
import 'package:appecomerce/screens/ScreenShop.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:appecomerce/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:appecomerce/config.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: MyAppConfig.appNavColorPrimary,
      ),
      home: EcomerceApp(),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        S.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
    );
  }
}

class EcomerceApp extends StatefulWidget {
  @override
  _EcomerceAppState createState() => _EcomerceAppState();
}

class _EcomerceAppState extends State<EcomerceApp> {
  int selectedTab = 0;

  var pages = [
    ScreenShop(),
    ScreenFavorites(),
    ScreenLocation(),
    ScreenLocation(),
    ScreenSettings()
  ];

  List<BottomNavigationBarItem> pagesContenido = <BottomNavigationBarItem>[
    ScreenShop().itemNavScreenShop(),
    ScreenFavorites().itemNavScreenFavorites(),
    ScreenLocation().itemNavScreenLocation(),
    ScreenLocation().itemNavScreenLocation(),
    ScreenSettings().itemNavScreenSettings()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ScreenHeader(),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: selectedTab,
          selectedItemColor: MyAppConfig.appNavColorPrimary,
          unselectedItemColor: MyAppConfig.appNavColorDisable,
          items: pagesContenido,
          onTap: (index) {
            setState(() {
              selectedTab = index;
            });
          },
          showUnselectedLabels: true,
          iconSize: 30,
        ),
        body: IndexedStack(
          index: selectedTab,
          children: pages,
        )
        //pages[selectedTab],
    );
  }
}