import 'package:flutter/material.dart';

class ScreenFavorites extends StatelessWidget {
  const ScreenFavorites({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Text('ScreenFavorites'),
          )
        ]
      ),
    );
  }

  BottomNavigationBarItem itemNavScreenFavorites(){
    return BottomNavigationBarItem(
      icon: Icon(Icons.favorite_border), 
      label: "Favorites"
    );
  }
}