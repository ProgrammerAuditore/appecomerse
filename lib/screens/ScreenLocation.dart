import 'package:flutter/material.dart';

class ScreenLocation extends StatelessWidget {
  const ScreenLocation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Text('ScreenLocation'),
          )
        ]
      ),
    );
  }

  BottomNavigationBarItem itemNavScreenLocation(){
    return BottomNavigationBarItem(
      icon: Icon(Icons.location_on), 
      label: "Location"
    );
  }
}