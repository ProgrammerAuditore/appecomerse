import 'package:flutter/material.dart';

class ScreenNotification extends StatelessWidget {
  const ScreenNotification({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Text('ScreenNotification'),
          )
        ]
      ),
    );
  }

  BottomNavigationBarItem itemNavScreenNotification(){
    return BottomNavigationBarItem(
      icon: Icon(Icons.notifications), 
      label: "Notificacions"
    );
  }
}