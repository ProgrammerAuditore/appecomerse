import 'package:flutter/material.dart';

class ScreenSettings extends StatelessWidget {
  const ScreenSettings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Text('ScreenSettings'),
          )
        ]
      ),
    );
  }

  BottomNavigationBarItem itemNavScreenSettings(){
    return BottomNavigationBarItem(
      icon: Icon(Icons.settings), 
      label: "Settings"
    );
  }
}