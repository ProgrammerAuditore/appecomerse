import 'package:appecomerce/generated/l10n.dart';
import 'package:flutter/material.dart';

class ScreenShop extends StatelessWidget {
  final List<Map> items = [
      {
        "title": "Kappa Velour",
        "category": "Bucket",
        "price": 5500,
        "descripcion": "Lo más nuevo de hoy",
        "image": "assets/nopicture.jpg"
      },
      {
        "title": "North Salty",
        "category": "Bucket",
        "price": 67000,
        "descripcion": "Este es para llevar",
        "image": "assets/food.jpg"
      },
      {
        "title": "Mest Takel",
        "category": "Bucket",
        "price": 67000,
        "descripcion": "Este es para aquí",
        "image": "assets/nopicture.jpg"
      },
    ];

  @override
  Widget build(BuildContext context) {
    
    return SafeArea(
          child: ListView.builder(
        itemBuilder: _buildListView,
        itemCount: items.length + 1,
      ));
  }

    Widget _buildListView(BuildContext context, int index) {
    if (index == 0)
      return Container(
        padding: EdgeInsets.all(20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              S.of(context).screenShopTitulo,
              style: TextStyle(fontSize: 18.0),
            ),
            Text(
                S.of(context).screenShopTituloBotonAll, 
                style: TextStyle(color: Colors.grey.shade500
              )
            ),
          ],
        ),
      );
    Map item = items[index - 1];
    return _buildShopItem(item);
  }

  Widget _buildShopItem(Map item) {
    return Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      margin: EdgeInsets.only(bottom: 20.0),
      height: 300,
      child: Row(
        children: <Widget>[
          Expanded(
              child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(item["image"]),
                    fit: BoxFit.cover
                ),
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey,
                      offset: Offset(5.0, 5.0),
                      blurRadius: 10.0)
                ]),
          )),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    item["title"],
                    style:
                        TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(item["category"],
                      style: TextStyle(color: Colors.grey, fontSize: 18.0)),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(item["descripcion"],
                      style: TextStyle(
                          fontSize: 18.0, color: Colors.grey, height: 1.5)),
                  SizedBox(
                    height: 10.0,
                  ),
                  Expanded(
                    child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("\$${item["price"].toString()}",
                                  style: const TextStyle(
                                    color: Colors.red,
                                    fontSize: 30.0,
                                  )
                                ),
                                IconButton(
                                  icon: const Icon(Icons.shopping_cart),
                                  onPressed: () {},
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      
                    ],
                  ) 
                  )
                ],
              ),
              margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(5.0, 5.0),
                        blurRadius: 10.0)
                  ]),
            ),
          )
        ],
      ),
    );
  }

  BottomNavigationBarItem itemNavScreenShop(){
    return BottomNavigationBarItem(
      icon: Icon(Icons.category), 
      label: "Shop"
    );
  }
  
}